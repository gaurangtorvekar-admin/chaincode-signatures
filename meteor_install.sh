#!/usr/bin/env bash
sudo apt-get update
sudo apt-get upgrade
curl https://install.meteor.com/ | sh
sudo apt-get install libfontconfig
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
sudo dpkg-reconfigure locales
sudo apt-get install build-essential
sudo apt-get install imagemagick ghostscript poppler-utils
sudo apt-get install libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++
cd attores/open_certs
meteor add npm-bcrypt
sudo meteor npm install

